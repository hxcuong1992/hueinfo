FastClick.attach(document.body);
//
var globalDevice={
    model:'',
    platform_cordova:'',
    platform_device:'',
    device_uuid:'',
    device_version:''
};

var module = angular.module('myApp', ['flipsnap','ngError','ngTouch','infinite-scroll','uiGmapgoogle-maps','ngDialog','ngCookies','ngRoute','panzoom','angular-carousel']);
module.config(['$routeProvider','$httpProvider', function($routeProvider,$httpProvider) {
    //
    $routeProvider
        .when('/',
        {
            templateUrl: 'partials/slider.html',
            controller: 'SliderCtrl'
        })
        .when('/festivalvn',{
            templateUrl:'partials/festival.html',
            controller:'FestivalCtrl'
        })
        .when('/locationvn',{
            templateUrl:'partials/location.html',
            controller:'LocationCtrl'
        })
        .when('/newsvn',{
            templateUrl:'partials/news.html',
            controller:'NewsCtrl'
        })
        .when('/mediavn',{
            templateUrl:'partials/media.html',
            controller:'MediaCtrl'
        })
        .when('/sharingvn',{
            templateUrl:'partials/sharing.html',
            controller:'SharingCtrl'
        })
        .when('/travelvn',{
            templateUrl:'partials/travelvhtml',
            controller:'TravelCtrl'
        })
    ;
    $routeProvider.otherwise({redirectTo: '/'});

    $httpProvider.interceptors.push(function($q, $location,$window) {
        return {
            'request': function (config) {
                config.headers = config.headers || {};
                if ($window.localStorage.token) {
                    config.headers.Authorization = 'Bearer ' + $window.localStorage.token;
                }
                return config;
            },
            'responseError': function(response) {
                if(response.status === 401 || response.status === 403 || response.status ===500) {
                    response.send({type:false});
                }
                return $q.reject(response);
            }
        };
    });

}]);

