(function(angular){
    'use strict';
    var myApp = angular.module('myApp');
    myApp.controller('FlowManagerController',function($location,$scope){

        $scope.colors = ["#fc0003", "#f70008", "#f2000d", "#ed0012", "#e80017", "#e3001c", "#de0021", "#d90026", "#d4002b", "#cf0030", "#c90036", "#c4003b", "#bf0040", "#ba0045", "#b5004a", "#b0004f", "#ab0054", "#a60059", "#a1005e", "#9c0063", "#960069", "#91006e", "#8c0073", "#870078", "#82007d", "#7d0082", "#780087", "#73008c", "#6e0091", "#690096", "#63009c", "#5e00a1", "#5900a6", "#5400ab", "#4f00b0", "#4a00b5", "#4500ba", "#4000bf", "#3b00c4", "#3600c9", "#3000cf", "#2b00d4", "#2600d9", "#2100de", "#1c00e3", "#1700e8", "#1200ed", "#0d00f2", "#0800f7", "#0300fc"];
        $scope.carouselIndex = 0;

      

        //setTimeout(function(){
        //    $scope.flipsnap.moveToPoint(15);
        //},6000)

        $scope.clickbar= function (a)
        {

            $scope.carouselIndex = a;

        }
        $scope.slides =[
            {
                id: 1,
                label: 'Cuong 1',
                img: 'slider1.png',
                color: $scope.colors[0],
                odd: false,
                description:'Festival Huế 2016 có chủ đề “710 năm Thuận Hóa - Phú Xuân - Thừa Thiên Huế; di sản văn hóa với hội nhập và phát triển” sẽ diễn ra từ ngày 29.4 đến ngày 4.5.2016.',
                name:'Festival ',
                href:'festivalvn'
            },
            {
                id: 2,
                label: 'Cuong 2',
                img: 'slider2.png',
                color: $scope.colors[1],
                odd: true,
                description:'Festival Huế 2016 có chủ đề “710 năm Thuận Hóa - Phú Xuân - Thừa Thiên Huế; di sản văn hóa với hội nhập và phát triển” sẽ diễn ra từ ngày 29.4 đến ngày 4.5.2016.',
                name:'Tin Tức',
                href:'festival'
            },
            {
                id: 3,
                label: 'Cuong 4',
                img: 'slider3.png',
                color: $scope.colors[2],
                odd: false,
                description:'Festival Huế 2016 có chủ đề “710 năm Thuận Hóa - Phú Xuân - Thừa Thiên Huế; di sản văn hóa với hội nhập và phát triển” sẽ diễn ra từ ngày 29.4 đến ngày 4.5.2016.',
                name:'Địa Điểm'
            },
            {
                id: 4,
                label: 'Cuong 5',
                img: 'slider4.png',
                color: $scope.colors[3],
                odd: true,
                description:'Festival Huế 2016 có chủ đề “710 năm Thuận Hóa - Phú Xuân - Thừa Thiên Huế; di sản văn hóa với hội nhập và phát triển” sẽ diễn ra từ ngày 29.4 đến ngày 4.5.2016.',
                name:'Du Lịch'
            },
            {
                id: 5,
                label: 'Cuong 5',
                img: 'slider4.png',
                color: $scope.colors[3],
                odd: true,
                description:'Festival Huế 2016 có chủ đề “710 năm Thuận Hóa - Phú Xuân - Thừa Thiên Huế; di sản văn hóa với hội nhập và phát triển” sẽ diễn ra từ ngày 29.4 đến ngày 4.5.2016.',
                name:'Huế Trong Tôi'
            },

            {
                id: 6,
                label: 'Cuong 5',
                img: 'slider4.png',
                color: $scope.colors[3],
                odd: true,
                description:'Festival Huế 2016 có chủ đề “710 năm Thuận Hóa - Phú Xuân - Thừa Thiên Huế; di sản văn hóa với hội nhập và phát triển” sẽ diễn ra từ ngày 29.4 đến ngày 4.5.2016.',
                name:'Thư Viện'
            },
            {
                id: 7,
                label: 'Cuong 5',
                img: 'slider4.png',
                color: $scope.colors[3],
                odd: true,
                description:'Festival Huế 2016 có chủ đề “710 năm Thuận Hóa - Phú Xuân - Thừa Thiên Huế; di sản văn hóa với hội nhập và phát triển” sẽ diễn ra từ ngày 29.4 đến ngày 4.5.2016.',
                name:'Liên hệ'
            }

        ];
    });

	 myApp.directive ("navBar", function () {
        return {
          restrict: 'A',
            require: 'ngModel',
            link: function (scope,el,attrs,ngModel) {
                var oldValue = ngModel.$viewValue;
                el.css("width",$(window).innerWidth());

				ngModel.$render = function () {
                    var newValue= ngModel.$viewValue;
                    if (isNaN(oldValue))
                    {
                        oldValue=newValue;
                        return;
                    }
                    oldValue=newValue;
                   var newPosition = $(el.children()[newValue]).offset().left + +el.scrollLeft();
                   el.animate ({
                      scrollLeft: newPosition
                  }, 200);
                   // el.transition({ x: -newPosition, y: 0 },200).promise().done( el.scrollLeft(newPosition).promise()
                   //     .done(el.transition({ x: 0, y: 0 },200)));

                }

            }

        };

    });
	
	 myApp.directive ("menuSpace", function () {
        return {

            restrict: 'A',
            link: function (scope,el,atts) {

                el.css ("width",$(window).innerWidth()/2
                );
            }
        };

    });
    myApp.directive('capitalize', function() {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, modelCtrl) {
                var capitalize = function(inputValue) {
                    if(inputValue == undefined) inputValue = '';
                    var capitalized = inputValue.toLowerCase();
                    if(capitalized !== inputValue) {
                        modelCtrl.$setViewValue(capitalized);
                        modelCtrl.$render();
                    }
                    return capitalized;
                };
                modelCtrl.$parsers.push(capitalize);
                capitalize(scope[attrs.ngModel]);  // capitalize initial value
            }
        };
    });



    angular.module('ngIOS9UIWebViewPatch', ['ng']).config(['$provide', function($provide) {
        'use strict';

        $provide.decorator('$browser', ['$delegate', '$window', function($delegate, $window) {

            if (isIOS9UIWebView($window.navigator.userAgent)) {
                return applyIOS9Shim($delegate);
            }

            return $delegate;

            function isIOS9UIWebView(userAgent) {
                return /(iPhone|iPad|iPod).* OS 9_\d/.test(userAgent) && !/Version\/9\./.test(userAgent);
            }

            function applyIOS9Shim(browser) {
                var pendingLocationUrl = null;
                var originalUrlFn= browser.url;

                browser.url = function() {
                    if (arguments.length) {
                        pendingLocationUrl = arguments[0];
                        return originalUrlFn.apply(browser, arguments);
                    }
                    return pendingLocationUrl || originalUrlFn.apply(browser, arguments);
                };

                window.addEventListener('popstate', clearPendingLocationUrl, false);
                window.addEventListener('hashchange', clearPendingLocationUrl, false);

                function clearPendingLocationUrl() {
                    pendingLocationUrl = null;
                }

                return browser;
            }
        }]);
    }]);

})(window.angular);