/**
 * Created by hoxuancuong on 11/29/15.
 */


(function(angular){
    var module = angular.module('myApp');
    module.controller('SliderCtrl', function($scope,$location){

        $scope.carouselIndex = $scope.$parent.carouselIndex;
        console.log('Begin::');
        console.log('$scope.$parent.slides::', $scope.$parent.slides);
        $scope.slides = $scope.$parent.slides;

        $scope.clickSliderButton = function(item){
            $location.path('/'+item.href)
        };


    });
})(window.angular);